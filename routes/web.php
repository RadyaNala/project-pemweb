<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\AlatController;
use App\Http\Controllers\SewaController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PengembalianController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('page.home');
});

Route::group(['middleware' => ['auth']], function () {
  Route::get('/kategori/create', [KategoriController::class, 'create']);
  //Tambah data ke database
  Route::post('/kategori', [KategoriController::class, 'store']);
  //Read
  //Menampilkan semua data
  Route::get('/kategori', [KategoriController::class, 'index']);
  //MENAMPILKAN DATA BERDASARKAN ID
  Route::get('/kategori/{id}', [KategoriController::class, 'show']);
  //Edit data
  Route::get('/kategori/{id}/edit', [KategoriController::class, 'edit']);
  //Update data
  Route::put('/kategori/{id}', [KategoriController::class, 'update']);
  //Delete data
  Route::delete('/kategori/{id}', [KategoriController::class, 'delete']);
});

//CRUD Profile
Route::resource('profile', ProfileController::class);

//CRUD Alat
Route::resource('alat', AlatController::class);

//Auth
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'indexAdmin'])->middleware('role:admin')->name('dashboard');

//CRUD SEWA
Route::resource('sewa', SewaController::class);

//CRUD PENGEMBALIAN
Route::resource('pengembalian', PengembalianController::class);