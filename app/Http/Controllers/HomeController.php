<?php

namespace App\Http\Controllers;

use App\Models\Alat;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $alat = Alat::all();
        return view('page.home', ['alat' => $alat]);
    }
    public function indexAdmin()
    {

        return view('page.dashboard');
    }
}