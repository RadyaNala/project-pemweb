<?php

namespace App\Http\Controllers;

use App\Models\Pengembalian;
use App\Models\Sewa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use File;

class PengembalianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pengembalian = Pengembalian::all();
        return view('pengembalian.tampil', ['pengembalian' => $pengembalian]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::id();
        $sewa = Sewa::where('id_user',$user)->first();
        return view('pengembalian.tambah', ['sewa' => $sewa, 'user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_sewa' => 'required',
            'keterangan' => 'required',
            'tgl_kembali' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png'
        ]);

        $newNamaImage = time() . '.' . $request->image->extension();

        //pindahkan file ke folder public
        $request->image->move(public_path('image'), $newNamaImage);

        //masukan data ke database
        $pengembalian = new Pengembalian();

        $pengembalian->id_sewa = $request['id_sewa'];
        $pengembalian->keterangan = $request['keterangan'];
        $pengembalian->tgl_kembali = $request['tgl_kembali'];
        $pengembalian->image = $newNamaImage;

        $pengembalian->save();
        return redirect('/pengembalian');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pengembalian = Pengembalian::find($id);
        return view('pengembalian.detail', ['pengembalian' => $pengembalian]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengembalian = Pengembalian::find($id);
        $user = Auth::id();
        $sewa = Sewa::where('id_user', $user)->first();
        return view('pengembalian.edit', ['pengembalian' => $pengembalian, 'sewa' => $sewa]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_sewa' => 'required',
            'keterangan' => 'required',
            'tgl_kembali' => 'required',
            'image' => 'mimes:jpeg,jpg,png'
        ]);

        $pengembalian = Pengembalian::find($id);

        $pengembalian->id_sewa = $request['id_sewa'];
        $pengembalian->keterangan = $request['keterangan'];
        $pengembalian->tgl_kembali = $request['tgl_kembali'];

        $pengembalian->save();

        if ($request->has('image')) {
            $path = 'image/';
            File::delete($path . $pengembalian->image);

            $newNamaImage = time() . '.' . $request->image->extension();

            $request->image->move(public_path('image'), $newNamaImage);

            $pengembalian->image = $newNamaImage;
        }
        $pengembalian->save();
        return redirect('/pengembalian');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengembalian = Pengembalian::find($id);

        $path = 'image/';
        File::delete($path . $pengembalian->image);

        $pengembalian->delete();

        return redirect('/pengembalian');
    }
}