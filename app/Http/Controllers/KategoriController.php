<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Kategori;

class KategoriController extends Controller
{
    public function create()
    {
        return view('kategori.tambah');
    }
    public function store(Request $request)
    {
        $request->validate([
            'kategori' => 'required'
        ]);
        DB::table('kategori')->insadert([
            'nama' => $request['kategori']
        ]);
        return redirect('/kategori');
    }
    public function index()
    {
        $kategori = DB::table('kategori')->get();
        return view('kategori.tampil', ['kategori' => $kategori]);
    }
    public function show($id)
    {
        // $kategori = DB::table('kategori')->find($id);
        $kategori = Kategori::find($id);
        return view('kategori.detail', ['kategori' => $kategori]);
    }
    //Edit data 
    public function edit($id)
    {
        $kategori = DB::table('kategori')->find($id);
        return view('kategori.edit', ['kategori' => $kategori]);
    }
    //Update data
    public function update($id, Request $request)
    {
        //validasi data
        $request->validate([
            'kategori' => 'required'
        ]);
        //mengupdate data
        DB::table('kategori')
            ->where('id', $id)
            ->update([
                'nama' => $request['kategori']
            ]);
        return redirect('/kategori');
    }
    //Delete data
    public function delete($id)
    {
        DB::table('kategori')->where('id', $id)->delete();
        return redirect('/kategori');
    }
}