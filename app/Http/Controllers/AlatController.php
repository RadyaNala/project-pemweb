<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Alat;
use File;
use Illuminate\Support\Facades\DB;

class AlatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alat = Alat::all();
        return view('alat.tampil', ['alat' => $alat]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('alat.tambah', ['kategori' => $kategori]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'keterangan' => 'required|min:5',
            'id_kategori' => 'required',
            'harga' => 'required',
            'image' => 'required|mimes:jpeg,jpg,png'
        ]);

        $newNamaImage = time() . '.' . $request->image->extension();

        //pindahkan file ke folder public
        $request->image->move(public_path('image'), $newNamaImage);

        //masukan data ke database
        $alat = new Alat;

        $alat->nama = $request['nama'];
        $alat->keterangan = $request['keterangan'];
        $alat->harga_sewa = $request['harga'];
        $alat->image = $newNamaImage;
        $alat->id_kategori = $request['id_kategori'];

        $alat->save();
        return redirect('/alat');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $alat = Alat::find($id);
        return view('alat.detail', ['alat' => $alat]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alat = Alat::find($id);
        $kategori = Kategori::all();

        return view('alat.edit', ['alat' => $alat, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'keterangan' => 'required|min:5',
            'id_kategori' => 'required',
            'harga' => 'required',
            'image' => 'mimes:jpeg,jpg,png'
        ]);
        $alat = Alat::find($id);

        $alat->nama = $request['nama'];
        $alat->keterangan = $request['keterangan'];
        $alat->id_kategori = $request['id_kategori'];
        $alat->harga_sewa = $request['harga'];

        $alat->save();

        if ($request->has('image')) {
            $path = 'image/';
            File::delete($path . $alat->image_alat);

            $newNamaImage = time() . '.' . $request->image->extension();

            $request->image->move(public_path('image'), $newNamaImage);

            $alat->image_alat = $newNamaImage;
        }
        $alat->save();
        return redirect('/alat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $alat = Alat::find($id);

        $path = 'image/';
        File::delete($path . $alat->image_alat);

        $alat->delete();

        return redirect('/alat');
    }
}