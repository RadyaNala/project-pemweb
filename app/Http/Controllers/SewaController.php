<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Alat;
use App\Models\Sewa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SewaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sewa = Sewa::all();
        return view('sewa.tampil', ['sewa' => $sewa]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user1 = Auth::user()->id;
        $user = User::all();
        $alat = Alat::all();
        return view('sewa.tambah', ['user' => $user, 'alat' => $alat, 'user1' => $user1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tgl_sewa' => ['required'],
            'tgl_kembali' => ['required'],
            'keterangan' => ['required'],
            'id_user' => ['required'],
            'id_alat' => ['required'],

        ]);
        $alat = Alat::find($request['id_alat'])->first();
        $sewa = new Sewa;

        $sewa->tgl_sewa = $request['tgl_sewa'];
        $sewa->tgl_kembali = $request['tgl_kembali'];
        $sewa->keterangan = $request['keterangan'];
        $sewa->harga_sewa = $alat->harga_sewa;
        $sewa->id_user = $request['id_user'];
        $sewa->id_alat = $request['id_alat'];

        $sewa->save();
        return redirect('/sewa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sewa = Sewa::find($id);
        $alat = Alat::all();
        $user = user::all();

        return view('sewa.edit', ['sewa' => $sewa, 'alat' => $alat, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tgl_sewa' => ['required'],
            'tgl_kembali' => ['required'],
            'keterangan' => ['required'],
            'id_user' => ['required'],
            'id_alat' => ['required'],

        ]);
        $sewa = Sewa::find($id);
        $alat = Sewa::find($sewa->id_alat)->first();
        $sewa->tgl_sewa = $request['tgl_sewa'];
        $sewa->tgl_kembali = $request['tgl_kembali'];
        $sewa->keterangan = $request['keterangan'];
        $sewa->harga_sewa = $alat->harga_sewa;
        $sewa->id_user = $request['id_user'];
        $sewa->id_alat = $request['id_alat'];

        $sewa->save();

        return redirect('/sewa')->with('success', 'Edit Answer Success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sewa = Sewa::find($id);
        $sewa->delete();

        return redirect('/sewa')->with('toast_warning', 'Delete Answer Success');
    }
}