<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
  public function index()
  {
    $profile = User::with('profile')->get();
    return view('profile.tampil', ['profile' => $profile]);
  }

  public function create()
  {
    return view('profile.tambah');
  }
  public function store(Request $request)
  {
    $request->validate([
      'name' => ['required', 'string', 'max:255'],
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      'password' => ['required', 'string', 'min:8', 'confirmed'],
      'alamat' => ['required'],
      'telpon' => ['required'],
    ]);
    $user = User::create([
      'name' => $request['name'],
      'email' => $request['email'],
      'password' => Hash::make($request['password']),
    ]);
    Profile::create([
      'alamat' => $request['alamat'],
      'telpon' => $request['telpon'],
      'id_user' => $user->id,
    ]);

    return redirect('/profile');
  }
  public function edit($id)
  {
    $profile = Profile::find($id);

    return view('profile.edit', ['profile' => $profile]);
  }
  public function update(Request $request, $id)
  {
    $request->validate([
      'name' => ['required', 'string', 'max:255'],
      'alamat' => ['required'],
      'telpon' => ['required'],
    ]);
    $profile = Profile::find($id);
    $user = User::where('id', $profile->id)->first();

    $user->name = $request['name'];
    $profile->alamat = $request['alamat'];
    $profile->telpon = $request['telpon'];

    $profile->save();
    $user->save();

    return redirect('/profile');
  }
  public function destroy($id)
  {
    $profile = Profile::find($id);
    $user = User::where('id', $profile->id)->first();

    $profile->delete();
    $user->delete();

    return redirect('/profile');
  }
}