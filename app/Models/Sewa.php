<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sewa extends Model
{
    use HasFactory;

    protected $table = 'sewa';

    protected $fillable = ['jumlah', 'tgl_sewa', 'tgl_kembali', 'keterangan', 'harga_sewa', 'id_user', 'id_alat'];

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }
    public function alat()
    {
        return $this->belongsTo(Alat::class, 'id_alat');
    }
    public function pengembalian()
    {
        return $this->hasOne(Pengembalian::class, 'id_sewa');
    }
}