<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengembalian extends Model
{
    use HasFactory;

    protected $table = 'pengembalian';

    protected $fillable = ['keterangan', 'image', 'tgl_kembali', 'id_sewa'];

    public function sewa()
    {
        return $this->belongsTo(Sewa::class, 'id_sewa');
    }
}