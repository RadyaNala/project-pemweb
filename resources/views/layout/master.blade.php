<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>TN Adventure</title>
  <link rel="shortcut icon" type="image/png" href="{{asset('/template/src/assets/images/logos/favicon.png')}}" />
  <link rel="stylesheet" href="{{asset('/template/src/assets/css/styles.min.css')}}" />
</head>

<body>
  <!--  Body Wrapper -->
  <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
    data-sidebar-position="fixed" data-header-position="fixed">
    <!-- Sidebar Start -->
    @include('partials.sidebar')
      <!-- End Sidebar scroll-->
    </aside>
    <!--  Sidebar End -->
    <!--  Main wrapper -->
    <div class="body-wrapper">
      @include('partials.navbar')
      <div class="container">
        <div class="row">
          <div class="col-lg-12 d-flex align-items-strech">
            <div class="card w-100">
              <div class="card-body">
                <div class="d-sm-flex d-block align-items-center justify-content-between mb-9">
                  <div class="mb-3 mb-sm-0">
                    <h2 class="card-title fw-semibold">@yield('title')</h2>
                  </div>
                </div>
                  @yield('konten')
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script src="{{asset('/template/src/assets/libs/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('/template/src/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('/template/src/assets/js/sidebarmenu.js')}}"></script>
  <script src="{{asset('/template/src/assets/js/app.min.js')}}"></script>
  <script src="{{asset('/template/src/assets/libs/apexcharts/dist/apexcharts.min.js')}}"></script>
  <script src="{{asset('/template/src/assets/libs/simplebar/dist/simplebar.js')}}"></script>
  <script src="{{asset('/template/src/assets/js/dashboard.js')}}"></script>
</body>

</html>