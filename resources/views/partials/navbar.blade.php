<div class="navbar-collapse justify-content-end px-4" id="navbarNav">
  <ul class="navbar-nav flex-row ms-auto align-items-center justify-content-end">
    @guest
        <p class="mx-4 my-auto">Silakan Login</p>
    @endguest
    @auth
    <a href="#" target="_blank" class="mx-4">{{Auth::user()->name}}</a>
    @endauth
    <li class="nav-item dropdown">
      <a class="nav-link nav-icon-hover" href="javascript:void(0)" id="drop2" data-bs-toggle="dropdown"
        aria-expanded="false">
        <img src="{{asset('/template/src/assets/images/profile/user-1.jpg')}}" alt="" width="35" height="35" class="rounded-circle">
      </a>
      <div class="dropdown-menu dropdown-menu-end dropdown-menu-animate-up" aria-labelledby="drop2">
        @guest
        <div class="message-body">
          <a href="/login" class="btn btn-outline-primary mx-3 mt-2 d-block">Login</a>
        </div>
        @endguest
        @auth
        <div class="message-body">
          <a href="javascript:void(0)" class="d-flex align-items-center gap-2 dropdown-item">
            <i class="ti ti-user fs-6"></i>
            <p class="mb-0 fs-3">My Profile</p>
          </a>
          <a href="javascript:void(0)" class="d-flex align-items-center gap-2 dropdown-item">
            <i class="ti ti-mail fs-6"></i>
            <p class="mb-0 fs-3">My Account</p>
          </a>
          <a href="javascript:void(0)" class="d-flex align-items-center gap-2 dropdown-item">
            <i class="ti ti-list-check fs-6"></i>
            <p class="mb-0 fs-3">My Task</p>
          </a>
          <a href="{{ route('logout') }}" class="btn btn-outline-danger mx-3 mt-2 d-block" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
        </div>
        @endauth
        
      </div>
    </li>
  </ul>
</div>