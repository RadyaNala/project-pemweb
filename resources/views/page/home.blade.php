@extends('layout.master')

@section('title')
Halaman Home
@endsection

@section('konten')
<p>Selamat Datang di Website TN Adventure!</p> 
    <div class="row">
      <div class="col-4">
        <div class="card">
          <img class="card-img-top" src="{{asset('/image/1686272609.jpg')}}" alt="Card image cap" width="145px" height="140px">
          <div class="card-body">
            <h2 class="card-title">Tenda xxx 4 Capacity</h2>
            <p class="card-text"></p>
            <div class="row">
            <a href="/alat" class="btn btn-block btn-info my-2">Detail</a>
          </div>
        </div>
      </div>
    </div>
        <div class="col-4">
          <div class="card">
            <img class="card-img-top" src="{{asset('/image/1686813083.jpg')}}" alt="Card image cap" width="145px" height="140px">
            <div class="card-body">
              <h2 class="card-title">Tenda xxx 5 Capacity</h2>
              <p class="card-text"></p>
              <div class="row">
              <a href="/alat/" class="btn btn-block btn-info my-2">Detail</a>
            </div>
          </div>
        </div>
      </div>

@endsection
