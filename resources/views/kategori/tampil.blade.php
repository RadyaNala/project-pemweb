@extends('layout.master')

@section('title')
    Halaman Data Kategori
@endsection

@section('konten')
    <table class="table table-light table-hover">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">Nama Kategori</th>
          <th scope="col"><center>Opsi</center></th>
        </tr>
      </thead>
      <tbody>
        @forelse ($kategori as $key=> $item)
            <tr>
              <td>{{$key +1}}</td>
              <td>{{$item->nama}}</td>
              <td>
                <center><a href="/kategori/{{$item->id}}/edit" class="btn btn-warning m-1">Edit</a>
                  <a href="/kategori/{{$item->id}}" class="btn btn-block btn-info my-2">Detail</a>
                <form action="/kategori/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger m-1">
                </form></center>
              </td>
            </tr>
          @empty
            <p>Data Kategori Kosong</p>
          @endforelse
      </tbody>
    </table>
    <a href="/kategori/create" class="btn btn-primary btn-sm my-2">Tambah Kategori</a>

@endsection