@extends('layout.master')

@section('title')
    Halaman Detail Kategori
@endsection

@section('konten')
<a href="/kategori" class="btn btn-block btn-info my-2">Kembali !</a>
<div class="row">
    @forelse ($kategori->alat as $item)
    <div class="col-4">
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('/image/' . $item->image)}}" alt="Card image cap">
            <div class="card-body">
              <h2>{{$item->nama}}</h2>
              <p class="card-text">{{$item->keterangan}}</p>
              <a href="/alat/{{$item->id}}" class="btn btn-block btn-info my-2">Detail Alat</a>
            </div>
          </div>
    </div>
    @empty
        <h5>Tidak ada alat pada kategori ini</h5>
    @endforelse
</div>


@endsection
