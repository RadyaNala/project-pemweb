@extends('layout.master')

@section('title')
    Halaman Edit Kategori
@endsection

@section('konten')
<form method="POST" action="/kategori/{{$kategori->id}}">
  @csrf
  @method("put")
  <div class="mb-3">
    <label for="kategori" class="form-label">Nama Kategori</label>
    <input type="text" class="form-control" id="kategori" name="kategori" value="{{$kategori->nama}}">
  </div>
  @error('kategori')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection