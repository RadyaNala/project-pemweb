@extends('layout.master')

@section('title')
    Halaman Tambah Kategori
@endsection

@section('konten')
<form method="POST" action="/kategori">
  @csrf
  <div class="mb-3">
    <label for="kategori" class="form-label">Nama Kategori</label>
    <input type="text" class="form-control" id="kategori" name="kategori">
  </div>
  @error('kategori')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection