@extends('layout.master')

@section('title')
    Halaman Edit User
@endsection

@section('konten')
<form method="POST" action="/profile/{{$profile->id}}">

  @csrf
  @method('put')
  <div class="row mb-3">
      <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

      <div class="col-md-6">
          <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$profile->user->name}}">

          @error('name')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
      </div>
  </div>
  <div class="row mb-3">
      <label for="alamat" class="col-md-4 col-form-label text-md-end">Alamat</label>

      <div class="col-md-6">
          <textarea name="alamat" id="alamat" class="form-control">{{$profile->alamat}}</textarea>

          @error('alamat')
              <span class="text-danger" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
      </div>
  </div>
  <div class="row mb-3">
      <label for="telpon" class="col-md-4 col-form-label text-md-end">Telpon</label>

      <div class="col-md-6">
          <input id="telpon" type="text" class="form-control @error('telpon') is-invalid @enderror" name="telpon" value="{{$profile->telpon}}">

          @error('telpon')
              <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
              </span>
          @enderror
      </div>
  </div>

  <div class="row mb-0">
      <div class="col-md-6 offset-md-4">
          <button type="submit" class="btn btn-warning">
              Edit
          </button>
      </div>
  </div>
</form>
@endsection