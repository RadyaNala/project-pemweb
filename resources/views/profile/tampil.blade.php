@extends('layout.master')

@section('title')
Halaman Data Profile
@endsection

@section('konten')
@if (!Auth::guest() && Auth::user()->hasRole('admin'))
<a href="/profile/create" class="btn btn-primary btn-sm my-2">Tambah Profile User</a>    
@endif

<table class="table table-light table-hover">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nama</th>
      <th scope="col">Email</th>
      <th scope="col">Alamat</th>
      <th scope="col">NoTelp</th>
      @if (!Auth::guest() && Auth::user()->hasRole('admin'))
      <th scope="col">Aksi</th>
      @endif
      
    </tr>
  </thead>
  <tbody>
    @forelse ($profile as $p)
      <tr>
        <td>{{$p->id}}</td>
        <td>{{$p->name}}</td>
        <td>{{$p->email}}</td>
        <td>{{$p->profile->alamat}}</td>
        <td>{{$p->profile->telpon}}</td>
        @if (!Auth::guest() && Auth::user()->hasRole('admin'))
        <td><a href="/profile/{{$p->id}}/edit" class="btn btn-warning m-1">Edit</a>
          <form action="/profile/{{$p->id}}" method="POST">
                @csrf
                @method('Delete')
                <input type="submit" value="Delete" class="btn btn-block btn-danger">
          </form>
      </td>
        @endif
      </tr>
      @empty
          <p>Data Kategori Kosong</p>
    @endforelse
  </tbody>
</table>
@endsection