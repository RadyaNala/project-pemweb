@extends('layout.master')

@section('title')
    Halaman Tambah Alat
@endsection

@section('konten')
<form method="POST" action="/alat" enctype="multipart/form-data">
  @csrf
  <div class="mb-3">
    <label for="nama" class="form-label">Nama Alat</label>
    <input type="text" class="form-control" id="nama" name="nama">
  </div>
  @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="Keterangan">Keterangan Alat</label>
    <textarea name="keterangan" id="Keterangan" cols="30" rows="10" class="form-control"></textarea>
  </div><br>
  @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="id_kategori">Kategori</label>
    <select name="id_kategori" id=""class="form-control">
      <option value="">---Pilih Kategori---</option>
      @forelse ($kategori as $item)
      <option value="{{$item->id}}">{{$item->nama}}</option>
      @empty
      <option value="">Belum ada data kategori</option>
      @endforelse
    </select>
  </div><br>
  @error('id_kategori')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="hargaSewa">Harga Sewa</label>
    <input type="number" class="form-control" id="hargaSewa" name='harga'>
  </div><br>
  @error('harga')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="GambarAlat">Gambar Alat</label>
    <input type="file" name="image" id="" class="form-control">
  </div><br>
  @error('image')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection