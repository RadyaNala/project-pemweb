@extends('layout.master')

@section('title')
Halaman Data Alat
@endsection

@section('konten')
    @if (!Auth::guest() && Auth::user()->hasRole('admin'))
    <div class="row">
    <div>
      <a href="/alat/create" class="btn btn-primary btn-sm mb-3">Tambah Alat</a>
    </div>
  </div>
    @else
        
    @endif
    
    
    <div class="row">
      @forelse ($alat as $item)
      <div class="col-4">
        <div class="card">
          <img class="card-img-top" src="{{asset('/image/' . $item->image)}}" alt="Card image cap" width="145px" height="140px">
          <div class="card-body">
            <h2 class="card-title">{{$item->nama}}</h2>
            <p class="card-text">{{Str::limit($item->keterangan,30)}}</p>
            <div class="row">
            <a href="/alat/{{$item->id}}" class="btn btn-block btn-info my-2">Detail</a>
            @if (!Auth::guest() && Auth::user()->hasRole('admin'))
            <a href="/alat/{{$item->id}}/edit" class="btn btn-block btn-warning">Edit</a>
            </div><br>
            <div>
              <form action="/alat/{{$item->id}}" method="POST">
                <div class="row">
                    @csrf
                    @method('Delete')
                    <input type="submit" value="Delete" class="btn btn-block btn-danger">
                </div>
              </form>
            </div>
            @else
            @endif
          </div>
        </div>
      </div>
      @empty
          <h5>Belum ada list alat</h5>
      @endforelse
    </div>
@endsection
