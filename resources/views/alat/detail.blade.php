@extends('layout.master')

@section('title')
Halaman Detail Alat
@endsection

@section('konten')
<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="{{asset('/image/' . $alat->image)}}" alt="Card image cap">
  <div class="card-body">
    <h2>{{$alat->nama}}</h2>
    <p class="card-text">{{$alat->keterangan}}</p>
    <a href="/alat" class="btn btn-block btn-info my-2">Kembali !</a>
  </div>
</div>
@endsection