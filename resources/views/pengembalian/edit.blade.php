@extends('layout.master')

@section('title')
    Halaman Pengembalian
@endsection

@section('konten')
@auth
<form method="POST" action="/pengembalian/{{$pengembalian->id}}" enctype="multipart/form-data">
  @csrf
  @method('put')
  <div class="form-group">
    <label for="id_sewa" class="form-label">ID Sewa</label>
    <input type="text" class="form-control" id='id_sewa' name='id_sewa' value="{{$sewa->id}}">
  </div>
  <div class="mb-3">
    <label for="tgl_kembali" class="form-label">Tanggal Kembali</label>
    <input type="date" class="form-control" id="tgl_kembali" name="tgl_kembali" value="{{$pengembalian->tgl_kembali}}">
  </div>
  @error('tgl_kembali')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="mb-3">
    <label for="keterangan" class="form-label">Keterangan</label>
    <textarea name="keterangan" id="keterangan" class="form-control">{{$pengembalian->keterangan}}</textarea>
  </div>
  @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="GambarAlat">Gambar Alat</label>
    <input type="file" name="image" id="GambarAlat" class="form-control">
  </div><br>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endauth
@endsection