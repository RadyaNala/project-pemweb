@extends('layout.master')

@section('title')
Halaman Detail
@endsection

@section('konten')
<div class="card" style="width: 18rem;">
  <img class="card-img-top" src="{{asset('/image/' . $pengembalian->image)}}" alt="Card image cap">
  <div class="card-body">
    <p class="card-text">{{$pengembalian->keterangan}}</p>
    <a href="/pengembalian" class="btn btn-block btn-info my-2">Kembali !</a>
  </div>
</div>
@endsection