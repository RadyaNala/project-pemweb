@extends('layout.master')

@section('title')
    Halaman Pengembalian
@endsection

@section('konten')
    @if (!Auth::guest() && Auth::user()->hasRole('admin'))
    <a href="/pengembalian/create" class="btn btn-primary btn-sm my-2">Tambah Pengembalian</a>
    @endif
    <table class="table table-light table-hover">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">ID Sewa</th>
          <th scope="col">Keterangan</th>
          <th scope="col">Tanggal Kembali</th>
          <th scope="col">Gambar</th>
          <th scope="col"><center>Opsi</center></th>
        </tr>
      </thead>
      <tbody>
        @forelse ($pengembalian as $key=> $item)
            <tr>
              <td>{{$key +1}}</td>
              <td>{{$item->id_sewa}}</td>
              <td>{{$item->keterangan}}</td>
              <td>{{$item->tgl_kembali}}</td>
              <td><img src="{{asset('/image/' . $item->image)}}" alt="image" width="100px" height="100px"></td>
              <td>
                <center><a href="/pengembalian/{{$item->id}}/edit" class="btn btn-warning m-1">Edit</a>
                  <a href="/pengembalian/{{$item->id}}" class="btn btn-block btn-info my-2">Detail</a>
                <form action="/pengembalian/{{$item->id}}" method="post">
                @csrf
                @method('delete')
                <input type="submit" value="Delete" class="btn btn-danger m-1">
                </form></center>
              </td>
            </tr>
          @empty
            <p>Data Pengembalian Kosong</p>
          @endforelse
      </tbody>
    </table>

@endsection