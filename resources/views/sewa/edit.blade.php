@extends('layout.master')

@section('title')
    Halaman Sewa
@endsection

@section('konten')
@auth
<form method="POST" action="/sewa/{{$sewa->id}}">
  @csrf
  @method('put')
  @if (!Auth::guest() && Auth::user()->hasRole('admin'))
  <div class="form-group">
    <label for="id_user" class="form-label">Nama User</label>
    <select name="id_user" id=""class="form-control">
      <option value="">--- Pilih User ---</option>
      @forelse ($user as $item)
      @if ($item->id === $sewa->id_user)
      <option value="{{$item->id}}" selected>{{$item->name}}</option>
      @else
      <option value="{{$item->id}}">{{$item->name}}</option>
      @endif
      @empty
      <option value="">Belum ada data user</option>
      @endforelse
    </select>
  </div><br>
  @else
  <div class="form-group">
    <label for="id_user" class="form-label">Nama User</label>
    <input type="text" class="form-control" value="{{Auth::user()->name}}">
  </div><br>
  @endif
  
  @error('id_user')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="id_alat" class="form-label">Alat</label>
    <select name="id_alat" id=""class="form-control">
      <option value="">--- Pilih Alat ---</option>
      @forelse ($alat as $item)
      @if ($item->id === $sewa->id_alat)
      <option value="{{$item->id}}" selected>{{$item->nama}}</option>
      @else
      <option value="{{$item->id}}">{{$item->nama}}</option>
      @endif
      @empty
      <option value="">Belum ada data alat</option>
      @endforelse
    </select>
  </div><br>
  @error('id_alat')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="mb-3">
    <label for="tgl_sewa" class="form-label">Tanggal Sewa</label>
    <input type="date" class="form-control" id="tgl_sewa" name="tgl_sewa" value="{{$sewa->tgl_sewa}}">
  </div>
  @error('tgl_sewa')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="mb-3">
    <label for="tgl_kembali" class="form-label">Tanggal Kembali</label>
    <input type="date" class="form-control" id="tgl_kembali" name="tgl_kembali" value="{{$sewa->tgl_kembali}}">
  </div>
  @error('tgl_sewa')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="mb-3">
    <label for="keterangan" class="form-label">Keterangan</label>
    <textarea name="keterangan" id="keterangan" class="form-control">{{$sewa->keterangan}}</textarea>
  </div>
  @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endauth

@endsection