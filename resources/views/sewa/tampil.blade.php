@extends('layout.master')

@section('title')
Halaman Data Sewa
@endsection

@section('konten')
    @auth
    {{-- <a href="/Answer/create" class="btn btn-primary btn-sm mb-3">Tambah Jawaban</a> --}}
    @endauth
    <div>
      <a href="/sewa/create" class="btn btn-primary btn-sm mb-3">Tambah Sewa</a>
    </div>      
    <table class="table">
      <thead>
        <tr>
          <th scope="col">No</th>
          <th scope="col">User</th>
          <th scope="col">Alat</th>
          <th scope="col">Keterangan</th>
          <th scope="col"><center>Aksi</center></th>
        </tr>
      </thead>
      <tbody>
        @forelse ($sewa as $key => $value)
          <tr>
            <td>{{$key +1}}</td>
            <td>{{$value->user->name}}</td>
            <td>{{$value->alat->nama}}</td>
            <td>{{$value->keterangan}}</td>
            <td>
              <center>
                <form action="/sewa/{{$value->id}}" method='post'>
                  @csrf
                  @method('delete')
                  @auth
                  <a href="/sewa/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  @endauth
                </form>
              </center>
            </td>
          </tr>
        @empty
            <tr>
              <td>Data Sewa Kosong</td>
            </tr>
        @endforelse
      </tbody>
    </table>
@endsection
