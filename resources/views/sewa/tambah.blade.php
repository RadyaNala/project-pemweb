@extends('layout.master')

@section('title')
    Halaman Sewa
@endsection

@section('konten')
@auth
<form method="POST" action="/sewa">
  @csrf
  @if (!Auth::guest() && Auth::user()->hasRole('admin'))
  <div class="form-group">
    <label for="id_user" class="form-label">Nama User</label>
    <select name="id_user" id=""class="form-control">
      <option value="">--- Pilih User ---</option>
      @forelse ($user as $u)
      <option value="{{$u->id}}">{{$u->name}}</option>
      @empty
      <option value="">Belum ada data User</option>
      @endforelse
    </select>
  </div><br>
  @else
  <div class="form-group d-none">
    <label for="id_user" class="form-label d-none">Nama User</label>
    <input type="text" class="form-control d-none" id='id_user' name='id_user' value="{{Auth::user()->id}}">
  </div>
  <div class="form-group">
    <label for="name" class="form-label">Nama User</label>
    <input type="text" class="form-control" value="{{Auth::user()->name}}">
  </div><br>
  @endif
  
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label for="id_alat" class="form-label">Alat</label>
    <select name="id_alat" id=""class="form-control">
      <option value="">--- Pilih Alat ---</option>
      @forelse ($alat as $item)
      <option value="{{$item->id}}">{{$item->nama}}</option>
      @empty
      <option value="">Belum ada data Alat</option>
      @endforelse
    </select>
  </div><br>
  @error('id_alat')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="mb-3">
    <label for="tgl_sewa" class="form-label">Tanggal Sewa</label>
    <input type="date" class="form-control" id="tgl_sewa" name="tgl_sewa">
  </div>
  @error('tgl_sewa')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="mb-3">
    <label for="tgl_kembali" class="form-label">Tanggal Kembali</label>
    <input type="date" class="form-control" id="tgl_kembali" name="tgl_kembali">
  </div>
  @error('tgl_sewa')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="mb-3">
    <label for="keterangan" class="form-label">Keterangan</label>
    <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
  </div>
  @error('keterangan')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endauth

@endsection