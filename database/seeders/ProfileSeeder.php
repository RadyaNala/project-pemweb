<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Profile;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profile::create([
            'alamat' => 'Purwokerto',
            'telpon' => '081335296744',
            'id_user' => 1
        ]);

        Profile::create([
            'alamat' => 'Purbalingga',
            'telpon' => '083213131324',
            'id_user' => 2
        ]);

        Profile::create([
            'alamat' => 'Purwokerto',
            'telpon' => '082387537463',
            'id_user' => 3
        ]);
    }
}
