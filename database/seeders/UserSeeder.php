<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'radya',
            'email' => 'radya@gmail.com',
            'password' => bcrypt('radya')
        ]);
        $admin->assignRole('admin');
        $admin = User::create([
            'name' => 'nisa',
            'email' => 'nisa@gmail.com',
            'password' => bcrypt('nisa')
        ]);
        $admin->assignRole('admin');

        $user = User::create([
            'name' => 'baqir',
            'email' => 'baqir@gmail.com',
            'password' => bcrypt('baqir')
        ]);
        $user->assignRole('user');
    }
}