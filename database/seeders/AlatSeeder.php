<?php

namespace Database\Seeders;

use App\Models\Alat;
use App\Models\Kategori;
use Illuminate\Database\Seeder;

class AlatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Kategori::create([
            'nama' => 'tenda'
        ]);

        Alat::create([
            'nama'=>'TendaXracing',
            'keterangan' => 'tenda anti racing',
            'harga_sewa' => 40000,
            'image' => "1686272609.jpg",
            'id_kategori' => 1
        ]);
    }
}
